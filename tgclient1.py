#!/usr/bin/env python3
# A simple script to print some messages.
import os
import sys
import time

from telethon import TelegramClient, events, utils

import logging
logging.basicConfig(level=logging.WARNING)


def get_env(name, message, cast=str):
    if name in os.environ:
        return os.environ[name]
    while True:
        value = input(message)
        try:
            return cast(value)
        except ValueError as e:
            print(e, file=sys.stderr)
            time.sleep(1)

# `pattern` is a regex, see https://docs.python.org/3/library/re.html
# Use https://regexone.com/ if you want a more interactive way of learning.
#
# "(?i)" makes it case-insensitive, and | separates "options".
# @events.register(events.NewMessage(pattern=r'(?i).*\b(Binance|Bitmex)\b'))
# async def handler(event):
#     sender = await event.get_sender()
#     name = utils.get_display_name(sender)
#     # print(name, 'said', event.text, '!')
#     print('Date = {}'.format(event.message.date))
#     step_0 = event.text.split('(')
#     step_1 = step_0[0]
#     print('Symbol = {}'.format(step_1))
#     step_2 = step_0[1].split(')')
#     print('Exchange = {}'.format(step_2[0]))
#     if not event.out:
#         if 'Binance' in event.text:
#             await event.message.reply('Binance or Bitmex found in message')


@events.register(events.NewMessage(pattern=r'(?i).*\b(Binance|Bitmex)\b'))
async def message_handler(event):
    """Callback method for received events.NewMessage"""

    # Note that message_handler is called when a Telegram update occurs
    # and an event is created. Telegram may not always send information
    # about the ``.sender`` or the ``.chat``, so if you *really* want it
    # you should use ``get_chat()`` and ``get_sender()`` while working
    # with events. Since they are methods, you know they may make an API
    # call, which can be expensive.
    chat = await event.get_chat()
    if event.is_group:
        if event.out:
            print('>> sent "{}" to chat {}'.format(
                event.text, utils.get_display_name(chat)
            ))
        else:
            print('<< {} @ {} sent "{}"'.format(
                utils.get_display_name(await event.get_sender()),
                utils.get_display_name(chat),
                event.text
            ))
    else:
        if event.out:

            print('>> "{}" to user {}'.format(
                event.text, utils.get_display_name(chat)
            ))
            # reply = await client.send_message(chat, 'So we get our message')
            await event.message.reply('Binance or Bitmex found in message')
        else:
            print('<< {} sent "{}"'.format(
                utils.get_display_name(chat), event.text
            ))


api_id: int = 0
api_hash: str = ''

client = TelegramClient(
    os.environ.get('TG_SESSION', 'mytgsession'),
    api_id,
    api_hash,
    proxy=None
)

with client:
    # This remembers the events.NewMessage we registered before
    client.add_event_handler(message_handler)
    print('(Press Ctrl+C to stop this)')
    client.run_until_disconnected()